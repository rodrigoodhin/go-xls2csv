
<div align="center">

![](/assets/logo_small.png "go-xls2csv")

<br>
<img src="https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white">
<img src="https://img.shields.io/badge/Microsoft_Excel-217346?style=for-the-badge&logo=microsoft-excel&logoColor=white">
<br><br>
<a href="#"><img src="https://img.shields.io/badge/test-success-green"></a>
<a href="#"><img src="https://img.shields.io/badge/build-passing-green"></a>
<a href="https://paypal.me/rodrigoodhin"><img src="https://img.shields.io/badge/donate-PayPal-blue"></a>
</div>

# GO-XLS2CSV

GO-XLS2CSV is a library written in pure Go providing the possibility to generate csv files from a xls or xlsx files. The package [xlsReader](https://github.com/shakinm/xlsReader) is used to read XLS files and the package [Excelize](https://github.com/qax-os/excelize) is used to read XLSX files.

&nbsp;
&nbsp;
&nbsp;

## Usage

Is it possible to import in a project

```go
import (
    "gitlab.com/rodrigoodhin/go-xls2csv/pkg/xls2csv"
)

func main() {
	
    // Convert XLS or XLSX file in a CSV file 
    xls2csv.Generate("./XLS/FILE/PATH", "./NEW/CSV/FILE/PATH", ",", "'", 0, false)
}
```

or you can build and execute
```
go build -o goXls2Csv
```

Usage of goXls2Csv
```
Example:
./goXls2Csv -x <XLSorXLSXFilePath> -c <CSVFIlePath> -s <Separator> -i <SheetIndex> -e -a <EncapsulateCharacter>

-x = Path to a XLSX file. MANDATORY
-c = Path to a CSV file. MANDATORY
-s = Separator used to create the new CSV file. Default: ;
-i = Index of sheet to convert. Default: 0
-e = Encapsulate determines if the strings in the CSV will be encapsulated
-a = Encapsulate character determines which character will be used if encapsulate param is true
```

Example:
```
./goXls2Csv -x Report.xls -c Report.csv 
./goXls2Csv -x Report.xls -c Report.csv -s , 
./goXls2Csv -x Report.xls -c Report.csv -s , -i 0
./goXls2Csv -x Report.xls -c Report.csv -s , -i 0 -e
./goXls2Csv -x Report.xls -c Report.csv -s , -i 0 -e -a "'"
```

&nbsp;
&nbsp;
&nbsp;

## LICENSE

[MIT License](/LICENSE)