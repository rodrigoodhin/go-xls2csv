package xls2csv

import (
	"fmt"
	"gitlab.com/rodrigoodhin/go-xls2csv/xls2csv"
	"io/ioutil"
)

func Generate(xlsFilePath, csvFilePath, separator, encapChar string, sheetIndex int, encapsulate bool) (err error) {

	if xlsFilePath == "" || csvFilePath == "" {
		return fmt.Errorf("XLS file path and CSV file path are mandatory parameters\n")
	}

	if separator == "" {
		separator = ";"
	}

	if encapChar == "" {
		encapChar = "\""
	}

	if sheetIndex < 0 {
		sheetIndex = 0
	}

	output, err := xls2csv.CSVFromXLS(xlsFilePath, separator, encapChar, sheetIndex, encapsulate)
	if output == "" {
		output, err = xls2csv.CSVFromXLSX(xlsFilePath, separator, encapChar, sheetIndex, encapsulate)
		if err != nil {
			return fmt.Errorf("Error on generating CSV file - %v\n", err.Error())
		}
	}

	err = ioutil.WriteFile(csvFilePath, []byte(output), 0644)
	if err != nil {
		return fmt.Errorf("Error on writing CSV file - %v\n", err.Error())
	}

	return
}
