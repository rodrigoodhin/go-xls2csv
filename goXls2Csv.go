package main

import (
	"flag"
	"fmt"
	"gitlab.com/rodrigoodhin/go-xls2csv/pkg/xls2csv"
	"os"
)

func main() {

	xlsPath := flag.String("x", "", "Path to a XLS or XLSX file")
	csvPath := flag.String("c", "", "Path to a CSV file")
	separator := flag.String("s", ";", "Separator used to create the new CSV file")
	sheetIndex := flag.Int("i", 0, "Index of sheet to convert, zero based")
	encapsulate := flag.Bool("e", false, "Encapsulate determines if the strings in the CSV will be encapsulated")
	encapChar := flag.String("a", "\"", "Encapsulate character determines which character will be used if encapsulate param is true")

	flag.Parse()

	var err error
	if *xlsPath == "" || *csvPath == "" {
		usage()
		return
	}

	err = xls2csv.Generate(*xlsPath, *csvPath, *separator, *encapChar, *sheetIndex, *encapsulate)
	if err != nil {
		fmt.Printf(err.Error())
		return
	}

}

func usage() {
	fmt.Printf("\nExample:\n%s -x <XLSorXLSXFilePath> -c <CSVFIlePath> -s <Separator> -i <SheetIndex> -e\n\n", os.Args[0])
	fmt.Println("-x = Path to a XLS or XLSX file. MANDATORY")
	fmt.Println("-c = Path to a CSV file. MANDATORY")
	fmt.Println("-s = Separator used to create the new CSV file. Default: ;")
	fmt.Println("-i = Index of sheet to convert. Default: 0")
	fmt.Println("-e = Encapsulate determines if the strings in the CSV will be encapsulated")
	fmt.Printf("-a = Encapsulate character determines which character will be used if encapsulate param is true\n\n")
}
