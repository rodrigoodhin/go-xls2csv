package xls2csv

import (
	"fmt"
	"github.com/shakinm/xlsReader/xls"
	"github.com/xuri/excelize/v2"
)

func CSVFromXLS(xlsFilePath, separator, encapChar string, sheetIndex int, encapsulate bool) (output string, err error) {

	encapsulated := ""
	if encapsulate {
		encapsulated = encapChar
	}

	if workbook, err := xls.OpenFile(xlsFilePath); err == nil {

		if sheet, err := workbook.GetSheet(sheetIndex); sheet != nil && err == nil {

			for i := 0; i < sheet.GetNumberRows(); i++ {
				rowString := ""

				if row, err := sheet.GetRow(i); row != nil && err == nil {

					for j, col := range row.GetCols() {
						if j > 0 {
							rowString = fmt.Sprintf("%v"+separator+""+encapsulated+"%v"+encapsulated+"", rowString, col.GetString())
						} else {
							rowString = fmt.Sprintf(encapsulated+"%v"+encapsulated, col.GetString())
						}
					}

					rowString = fmt.Sprintf("%v\n", rowString)
					output += rowString
				}
			}
		}
	}

	return
}

func CSVFromXLSX(xlsFilePath, separator, encapChar string, sheetIndex int, encapsulate bool) (output string, err error) {

	xlsFile, err := excelize.OpenFile(xlsFilePath)
	if err != nil {
		return "", err
	}

	sheetList := xlsFile.GetSheetList()
	rows, err := xlsFile.GetRows(sheetList[sheetIndex])
	if err != nil {
		fmt.Printf(err.Error())
		return
	}

	sheetLen := len(sheetList)
	switch {
	case sheetLen == 0:
		fmt.Printf("This XLSX file contains no sheets.\n")
		return "", nil
	case sheetIndex >= sheetLen:
		fmt.Printf("No sheet %d available, please select a sheet between 0 and %d\n", sheetIndex, sheetLen-1)
		return "", nil
	}

	encapsulated := ""
	if encapsulate {
		encapsulated = encapChar
	}

	for _, row := range rows {
		rowString := ""
		if row != nil {
			for cellIndex, cell := range row {
				if cellIndex > 0 {
					rowString = fmt.Sprintf("%s"+separator+""+encapsulated+"%s"+encapsulated+"", rowString, cell)
				} else {
					rowString = fmt.Sprintf(encapsulated+"%s"+encapsulated, cell)
				}
			}
			rowString = fmt.Sprintf("%s\n", rowString)
			output += rowString
		}
	}
	return
}
